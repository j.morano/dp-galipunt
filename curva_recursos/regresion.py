# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np


workers = [0, 4,4,4,4, 6,6,6,6, 7,7,7,7, 9,9,9,9, 10,10,10,10,
           10,10,10,10, 10,10,10,10, 10,10,10,10, 10,10,10,10,
           10,10,10,10, 8,8,8,8, 8,8,8,8, 6,6,6,6, 4,4,4,4,
           4,4,4,4, 0,0,0,0]

weeks = [i for i in range(len(workers))]


plt.figure()
poly = np.polyfit(weeks,workers,7)
poly_y = np.poly1d(poly)(weeks)
plt.plot(weeks,poly_y, 'indigo', label='Interpolación')
plt.step(weeks,workers, '-go', label='Trabajadores por semana', markersize=3)
plt.ylim((-1,16))
plt.xlim((-1,65))
plt.vlines([24, 48, 64], -0.5, 11, colors='b', label='Hitos de entrega')
plt.xlabel('Semana')
plt.ylabel('Nº de trabajadores')
plt.title('Curva de recursos')
plt.legend(loc='upper left')
plt.savefig('curva_recursos.pdf')
# plt.show()